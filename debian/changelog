elpa-subed (1.2.25-1) unstable; urgency=medium

  * New upstream version 1.2.25
  * Drop fix-screenshot-path.patch; applied upstream

 -- Xiyue Deng <manphiz@gmail.com>  Thu, 30 Jan 2025 22:16:47 -0800

elpa-subed (1.2.22-1) unstable; urgency=medium

  * New upstream version 1.2.22
  * Add d/salsa-ci.yml with default template
  * Use default gz compression in d/gbp.conf to be consistent with
    upstream

 -- Xiyue Deng <manphiz@gmail.com>  Sat, 21 Dec 2024 14:39:22 -0800

elpa-subed (1.2.21-1) unstable; urgency=medium

  * New upstream version
  * Refresh patches for the new version
  * Mark patch as "Forwarded: not-needed"

 -- Xiyue Deng <manphiz@gmail.com>  Thu, 12 Dec 2024 23:43:16 -0800

elpa-subed (1.2.17-1) unstable; urgency=medium

  * New upstream version

 -- Xiyue Deng <manphiz@gmail.com>  Sun, 08 Dec 2024 03:11:59 -0800

elpa-subed (1.2.16-1) unstable; urgency=medium

  * New upstream version (Closes: #1084797)
  * Add d/gbp.conf matching current practice
  * Modernize d/watch with substitute strings to be more robust
  * Add buttercup to Build-Depends to enable tests
  * Add ffmpeg to Build-Depends which is required by the tests
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse
  * Remove unused license definitions for GPL-2+
  * Update standards version to 4.7.0; no changes needed
  * Add `Rules-Requires-Root: no' in d/control
  * Add `Testsuite: autopkgtest-pkg-elpa' in d/control
  * Add myself to uploaders in d/control
  * Update copyright year and add myself in d/copyright

 -- Xiyue Deng <manphiz@gmail.com>  Tue, 22 Oct 2024 10:54:57 -0700

elpa-subed (1.0.29-3) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.5.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 25 Jul 2024 19:07:53 +0900

elpa-subed (1.0.29-2) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.3.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 25 Jul 2024 11:30:04 +0900

elpa-subed (1.0.29-1) unstable; urgency=medium

  * New upstream version
    - upstream changed from Random User to Sacha Chua
  * Add debian/watch

 -- Martin <debacle@debian.org>  Sat, 18 Feb 2023 09:03:03 +0000

elpa-subed (0.0.1+git20210228.49ddccc-2) unstable; urgency=medium

  * Source-only upload

 -- Martin <debacle@debian.org>  Tue, 09 Mar 2021 19:00:30 +0000

elpa-subed (0.0.1+git20210228.49ddccc-1) unstable; urgency=medium

  * Initial package (Closes: #984640)

 -- Martin <debacle@debian.org>  Sat, 06 Mar 2021 19:57:42 +0000
